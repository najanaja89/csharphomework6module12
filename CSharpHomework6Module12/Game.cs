﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework6Module12
{
    public class Game
    {
        public delegate void GameStartDelegate(string message);
        public event GameStartDelegate Winner;
        private Random randr;
        private Truck truck1;
        private Truck truck2;
        private Truck truck3;
        private SimpleCar car1;
        private SimpleCar car2;
        private SimpleCar car3;
        private SportCar sportCar1;
        private SportCar sportCar2;
        private SportCar sportCar3;

        public double Distance { set; get; }
        public Game(double distance)
        {
            randr = new Random();
            truck1 = new Truck("Truck1", randr.Next(200, 500));
            truck2 = new Truck("Truck2", randr.Next(200, 500));
            truck3 = new Truck("Truck3", randr.Next(200, 500));
            car1 = new SimpleCar("Car1", randr.Next(500, 700));
            car2 = new SimpleCar("Car2", randr.Next(500, 700));
            car3 = new SimpleCar("Car3", randr.Next(500, 700));
            sportCar1 = new SportCar("SportCar1", randr.Next(700, 1000));
            sportCar2 = new SportCar("SportCar2", randr.Next(700, 1000));
            sportCar3 = new SportCar("SportCar3", randr.Next(700, 1000));

            Distance = distance;
        }

        public void GameStart()
        {
            Dictionary<string, double> winner = new Dictionary<string, double>()
            {
                { truck1.Name, truck1.Move(Distance)},
                { truck2.Name, truck2.Move(Distance)},
                { truck3.Name, truck3.Move(Distance)},
                { car1.Name, car1.Move(Distance)},
                { car2.Name, car2.Move(Distance)},
                { car3.Name, car3.Move(Distance)},
                { sportCar1.Name, sportCar1.Move(Distance)},
                { sportCar2.Name, sportCar2.Move(Distance)},
                { sportCar3.Name, sportCar3.Move(Distance)}

            };

            var win = winner.OrderBy(i => i.Value).First();
            Winner($"Winner is {win.Key} with time {win.Value}");
        }

    }
}
