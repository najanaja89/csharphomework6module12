﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework6Module12
{
    public class Truck : Car
    {
        public override double Speed { get; set; }
        public override string Name { get; set; }

        public Truck(string name, double speed)
        {
            Name = name;
            Speed = speed;
        }

        public override double Move(double distance)
        {
            double t = distance / Speed;
            return t;
        }
    }
}
