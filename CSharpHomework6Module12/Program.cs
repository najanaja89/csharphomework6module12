﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework6Module12
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game(100);
            game.Winner += ShowWinner;
            game.GameStart();

            //Console.ReadLine();
        }

        private static void ShowWinner(string m)
        {
            System.Windows.Forms.MessageBox.Show(m);
        }

    }

}
