﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework6Module12
{
    public class SimpleCar : Car
    {
        Random rand;
        public override double Speed { get; set; }
        public override string Name { get; set; }

        public SimpleCar(string name, double speed)
        {
            rand = new Random();
            Speed = speed;
            Name = name;
        }

        public override double Move(double distance)
        {
            double t = distance / Speed;
            return t;
        }
    }
}
