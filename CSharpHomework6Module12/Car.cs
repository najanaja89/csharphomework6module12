﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework6Module12
{
    public abstract class Car
    {
        public abstract string Name { set; get; }

        public abstract double Speed { set; get; }

        public abstract double Move(double distance);

    }
}
